Espresso Responder
==================

Ease the creation of PSR-7 responses with this wrapper for PSR-17 HTTP Factories.

## Installation
Simply run `composer require espresso/responder`.

## Why this package?
PSR-17 HTTP Factories are an excellent solution to the problem of tightly coupling your app/library
to an specific PSR-7 implementation. However, response creation using factories also have some
drawbacks that this library tries to solve:

1. Response creation can become really verbose.
2. Some complex responses may need up to three extra dependencies (ResponseFactoryInterface, UriFactoryInterface and
StreamFactoryInterface) to be injected in a class.
3. Creating similar responses in multiple places leads to a lot of code duplication,
which makes code harder to maintain.

Responder solves this problem by creating the `Responder` interface that defines common and simple
response creation methods, and provides an implementation that wraps some of the PSR-17 HTTP
Factories. Also, provides a `ResponderAware` interface along with a trait that you can use to tell
DI Containers to perform setter injection of a `Responder` implementation, keeping your constructor
clean and dependency free.

## Usage

To use responder with, for example, Zend Diactoros:

```php
<?php

// We create the instance
$responder = new \Espresso\Responder\Psr\PsrResponder(
    new \Zend\Diactoros\ResponseFactory(),
    new \Zend\Diactoros\StreamFactory(),
    new \Zend\Diactoros\UriFactory()
);

// Responder can provide html responses
$responder->html('This is a not found html response.', 404);

// Redirect responses as well
$responder->redirect('/redirect/to/uri');

// Or simple json ones.
$responder->json([
    'msg' => 'This is a 500 json error response'
], 500);

// You can also pass objects to the json method. These will be normalized and
// casted to a json string using an optionally defined ResponderNormalizer instance.
$responder->setNormalizer($normalizerInstance);
$responder->json($object, 400);
// The object can also be an instance of \JsonSerializable.

// You can also create file responses.
$responder->file('/path/to/file.ext', 'content-type');

// If you don't know the file extension in advance, you can provide
// an instance of ResponderMime that will guess it out. It takes the file name as parameter and
// it must return the content-type to be injected in the request.
$responder->setMime($mimeInstance);
$responder->file('/a/file/with/dynamic/extension.file');

// You can also download a file
$responder->download('/path/to/file/to/download');

// Lastly, you can render a template providing an implementation of ResponderTemplating
$responder->setTemplating($templatingInstance);
$responder->render('templateName', [
   'var' => 'value' 
]);
```