<?php

namespace Espresso\Responder;

/**
 * Class UnsetResponderException
 * @package Espresso\Responder
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class UnsetResponderException extends \RuntimeException
{
    /**
     * UnsetResponderException constructor.
     * @param string $class
     * @param string $method
     */
    public function __construct(string $class, string $method)
    {
        parent::__construct(sprintf(
            'Unable to call %s method of %s class. A Responder instance must be set first using the %s::setResponder method.',
            $method,
            $class,
            $class
        ));
    }
}