<?php

namespace Espresso\Responder;

use JsonSerializable;
use Psr\Http\Message\ResponseInterface;

/**
 * The main goal of the responder is to ease the creation of PSR-7 responses.
 *
 * This aims to be a service that can be easily included in PSR-7 handlers and
 * middleware. How the responder renders templates or serializes objects to
 * json is an implementation detail.
 *
 * This comes to solve a design decision in PSR-7, the lack of common factory methods
 * for common content-types and actions related to responses.
 *
 * @package Espresso\Responder
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
interface Responder
{
    /**
     * Creates an html response.
     *
     * @param string $html
     * @param int    $status
     * @return ResponseInterface
     */
    public function html(string $html = '', int $status = 200): ResponseInterface;

    /**
     * Creates a json response.
     *
     * @param string|array|JsonSerializable $data
     * @param int $status
     * @return ResponseInterface
     */
    public function json($data = [], int $status = 200): ResponseInterface;

    /**
     * Renders an html template as a response.
     *
     * @param string $template
     * @param array $context
     * @param int $status
     * @return ResponseInterface
     */
    public function render(string $template, array $context = [], int $status = 200): ResponseInterface;

    /**
     * Returns a file as a response.
     *
     * Client code CAN send a Content-Type and implementations MUST prioritize this
     * as the content type to include in the response.
     *
     * If no content type is provided, implementations must provide mechanisms for
     * guessing it.
     *
     * @param string $path
     * @param string $contentType
     * @return ResponseInterface
     */
    public function file(string $path, string $contentType = null): ResponseInterface;

    /**
     * @param string $path
     * @param string|null $contentType
     * @param string|null $downloadName
     * @return ResponseInterface
     */
    public function download(string $path, string $contentType = null, string $downloadName = null): ResponseInterface;

    /**
     * Returns a redirect response.
     *
     * @param string $uri
     * @return ResponseInterface
     */
    public function redirect(string $uri): ResponseInterface;
}