<?php

namespace Espresso\Responder;

use Psr\Http\Message\ResponseInterface;

/**
 * This traits proxies calls to the underlying responder.
 *
 * @package Espresso\Responder
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
trait ResponderTrait
{
    /**
     * @var Responder
     */
    protected $responder;

    /**
     * @param Responder $responder
     */
    public function setResponder(Responder $responder): void
    {
        $this->responder = $responder;
    }

    /**
     * @param string $html
     * @param int $status
     * @return ResponseInterface
     */
    public function html(string $html = '', int $status = 200): ResponseInterface
    {
        $this->ensureResponder(__METHOD__);
        return $this->responder->html($html, $status);
    }

    /**
     * @param $data
     * @param int $status
     * @return ResponseInterface
     */
    public function json($data = [], int $status = 200): ResponseInterface
    {
        $this->ensureResponder(__METHOD__);
        return $this->responder->json($data, $status);
    }

    /**
     * @param string $template
     * @param array $context
     * @param int $status
     * @return ResponseInterface
     */
    public function render(string $template, array $context = [], int $status = 200): ResponseInterface
    {
        $this->ensureResponder(__METHOD__);
        return $this->responder->render($template, $context, $status);
    }

    /**
     * @param string $path
     * @param string|null $contentType
     * @return ResponseInterface
     */
    public function file(string $path, string $contentType = null): ResponseInterface
    {
        $this->ensureResponder(__METHOD__);
        return $this->responder->file($path, $contentType);
    }

    /**
     * @param string $path
     * @param string|null $contentType
     * @param string|null $downloadName
     * @return ResponseInterface
     */
    public function download(string $path, string $contentType = null, string $downloadName = null): ResponseInterface
    {
        $this->ensureResponder(__METHOD__);
        return $this->responder->download($path, $contentType, $downloadName);
    }

    /**
     * @param string $uri
     * @return ResponseInterface
     */
    public function redirect(string $uri): ResponseInterface
    {
        $this->ensureResponder(__METHOD__);
        return $this->responder->redirect($uri);
    }

    /**
     * @param string $method
     */
    protected function ensureResponder(string $method): void
    {
        if ($this->responder === null) {
            throw new UnsetResponderException(__CLASS__, $method);
        }
    }
}