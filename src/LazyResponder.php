<?php

namespace Espresso\Responder;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class LazyResponder
 *
 * Description of what this class does goes here.
 *
 * @author Matias Navarro Carter <mnavarro@option.cl>
 */
class LazyResponder implements Responder
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var string
     */
    private $responderService;

    /**
     * LazyResponder constructor.
     * @param ContainerInterface $container
     * @param string $responderService
     */
    public function __construct(ContainerInterface $container, string $responderService)
    {
        $this->container = $container;
        $this->responderService = $responderService;
    }

    /**
     * @param string $html
     * @param int $status
     * @return ResponseInterface
     */
    public function html(string $html = '', int $status = 200): ResponseInterface
    {
        return $this->responder()->html($html, $status);
    }

    /**
     * @param array $data
     * @param int $status
     * @return ResponseInterface
     */
    public function json($data = [], int $status = 200): ResponseInterface
    {
        return $this->responder()->json($data, $status);
    }

    /**
     * @param string $template
     * @param array $context
     * @param int $status
     * @return ResponseInterface
     */
    public function render(string $template, array $context = [], int $status = 200): ResponseInterface
    {
        return $this->responder()->render($template, $context, $status);
    }

    /**
     * @param string $path
     * @param string|null $contentType
     * @return ResponseInterface
     */
    public function file(string $path, string $contentType = null): ResponseInterface
    {
        return $this->responder()->file($path, $contentType);
    }

    /**
     * @param string $path
     * @param string|null $contentType
     * @param string|null $downloadName
     * @return ResponseInterface
     */
    public function download(string $path, string $contentType = null, string $downloadName = null): ResponseInterface
    {
        return $this->responder()->download($path, $contentType, $downloadName);
    }

    /**
     * @param string $uri
     * @return ResponseInterface
     */
    public function redirect(string $uri): ResponseInterface
    {
        return $this->responder()->redirect($uri);
    }

    /**
     * @return Responder
     */
    protected function responder(): Responder
    {
        return $this->container->get($this->responderService);
    }
}