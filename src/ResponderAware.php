<?php

namespace Espresso\Responder;

/**
 * This makes a class aware of the responder.
 *
 * This is mainly provided so it can be easy to inject a Responder instance
 * to services implementing this interface using a DI Container.
 *
 * This avoids polluting the constructor with unnecessary dependencies for a service
 * that should be rather transparent. This is meant to be used along with the
 * ResponderTrait.
 *
 * @package Espresso\Responder
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
interface ResponderAware
{
    /**
     * @param Responder $responder
     */
    public function setResponder(Responder $responder): void;
}