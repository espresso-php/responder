<?php

namespace Espresso\Responder\Psr;

/**
 * Interface ResponderNormalizer
 *
 * Description of what this class does goes here.
 *
 * @author Matias Navarro Carter <mnavarro@option.cl>
 */
interface ResponderNormalizer
{
    /**
     * @param $data
     * @return array
     */
    public function normalize($data): array;
}