<?php

namespace Espresso\Responder\Psr;

/**
 * Interface ResponderTemplating
 *
 * Description of what this class does goes here.
 *
 * @author Matias Navarro Carter <mnavarro@option.cl>
 */
interface ResponderTemplating
{
    /**
     * @param string $template
     * @param array $context
     * @return string
     */
    public function render(string $template, array $context = []): string;
}