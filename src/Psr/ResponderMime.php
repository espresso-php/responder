<?php

namespace Espresso\Responder\Psr;

/**
 * Interface ResponderMime
 *
 * Description of what this interface is for goes here.
 *
 * @author Matias Navarro Carter <mnavarro@option.cl>
 */
interface ResponderMime
{
    /**
     * @param string $filename
     * @return string
     */
    public function find(string $filename): string;
}