<?php

namespace Espresso\Responder\Psr;

use Espresso\Responder\Responder;
use InvalidArgumentException;
use JsonSerializable;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;

/**
 * Class PsrResponder
 * @package Espresso\Responder
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class PsrResponder implements Responder
{
    /**
     * @var ResponseFactoryInterface
     */
    protected $responseFactory;
    /**
     * @var StreamFactoryInterface
     */
    protected $streamFactory;
    /**
     * @var UriFactoryInterface
     */
    protected $uriFactory;
    /**
     * @var ResponderTemplating
     */
    protected $templating;
    /**
     * @var ResponderMime
     */
    protected $mime;
    /**
     * @var ResponderNormalizer
     */
    protected $normalizer;

    /**
     * PsrResponder constructor.
     * @param ResponseFactoryInterface $responseFactory
     * @param StreamFactoryInterface   $streamFactory
     * @param UriFactoryInterface      $uriFactory
     */
    public function __construct(
        ResponseFactoryInterface $responseFactory,
        StreamFactoryInterface $streamFactory,
        UriFactoryInterface $uriFactory
    ) {
        $this->responseFactory = $responseFactory;
        $this->streamFactory = $streamFactory;
        $this->uriFactory = $uriFactory;
    }

    /**
     * @param ResponderTemplating $templating
     */
    public function setTemplating(ResponderTemplating $templating): void
    {
        $this->templating = $templating;
    }

    /**
     * @param ResponderMime $mime
     */
    public function setMime(ResponderMime $mime): void
    {
        $this->mime = $mime;
    }

    /**
     * @param ResponderNormalizer $normalizer
     */
    public function setNormalizer(ResponderNormalizer $normalizer): void
    {
        $this->normalizer = $normalizer;
    }

    /**
     * @param string $path
     * @param string $contentType
     * @return ResponseInterface
     */
    public function file(string $path, string $contentType = null): ResponseInterface
    {
        if ($contentType === null && $this->mime === null) {
            $contentType = mime_content_type($path);
        }

        if ($contentType === null && $this->mime !== null) {
            $contentType = $this->mime->find($path);
        }

        $stream = $this->streamFactory->createStreamFromFile($path);
        $response = $this->responseFactory->createResponse()
            ->withBody($stream)
            ->withHeader('Content-Type', $contentType);

        if (($size = $stream->getSize()) === null) {
            return $response->withHeader('Transfer-Encoding', 'chunked');
        }
        return $response->withHeader('Content-Length', $size);
    }

    /**
     * @param string $path
     * @param string|null $contentType
     * @param string|null $downloadName
     * @return ResponseInterface
     */
    public function download(string $path, string $contentType = null, string $downloadName = null): ResponseInterface
    {
        $response = $this->file($path, $contentType);
        return $response->withHeader('Content-Disposition', sprintf(
            'attachment; filename=%s',
            $downloadName ?? basename($path)
        ));
    }

    /**
     * @param string|array|JsonSerializable $data
     * @param int $status
     * @return ResponseInterface
     */
    public function json($data = [], int $status = 200): ResponseInterface
    {
        if (!(is_array($data) || $data instanceof JsonSerializable || is_string($data) || is_object($data))) {
            throw new InvalidArgumentException('$data needs to be an array, an object, a json string or an instance of \JsonSerializable');
        }
        if (is_object($data) && !$data instanceof JsonSerializable && $this->normalizer !== null) {
            $data = $this->normalizer->normalize($data);
        }
        if (!is_string($data)) {
            $data = json_encode($data);
        }
        return $this->responseFactory->createResponse($status)
            ->withBody($this->streamFactory->createStream($data))
            ->withHeader('Content-Type', 'application/json; charset=utf8;');
    }

    /**
     * @param string $html
     * @param int    $status
     * @return ResponseInterface
     */
    public function html(string $html = '', int $status = 200): ResponseInterface
    {
        return $this->responseFactory->createResponse($status)
            ->withBody($this->streamFactory->createStream($html))
            ->withHeader('Content-Type', 'text/html; charset=utf8;');
    }

    /**
     * @param string $template
     * @param array  $context
     * @param int    $status
     * @return ResponseInterface
     */
    public function render(string $template, array $context = [], int $status = 200): ResponseInterface
    {
        if ($this->templating === null) {
            throw new RuntimeException('You need to set a render callable before using the render method.');
        }
        return $this->html($this->templating->render($template, $context), $status);
    }

    /**
     * @param string $uri
     * @return ResponseInterface
     */
    public function redirect(string $uri): ResponseInterface
    {
        return $this->responseFactory->createResponse(302)
            ->withHeader('Location', $uri);
    }
}